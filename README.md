const int middleIRPin = A0;
const int middleEchoPin = A1; 
const int leftIRPin  = 13;
const int rightIRPin = 12;
const int rightPulseWM = 11;
const int leftPulseWM = 10;
const int bottomRightIR  = 9;
const int bottomLeftIR = 8;
const int rightMotor2 = 7;
const int rightMotor1 = 6;
const int leftMotor2 = 5;
const int leftMotor1 = 4;





void setup()
{
  pinMode(middleIRPin,   INPUT);
  pinMode(middleEchoPin, INPUT);
  pinMode(leftIRPin,     INPUT);
  pinMode(rightIRPin,    INPUT);
  pinMode(bottomLeftIR,  INPUT);
  pinMode(bottomRightIR, INPUT);
  pinMode(leftMotor1,    OUTPUT);
  pinMode(leftMotor2,    OUTPUT);
  pinMode(rightMotor1,   OUTPUT);
  pinMode(rightMotor2,   OUTPUT);
  pinMode(leftPulseWM,   OUTPUT);
  pinMode(rightPulseWM,  OUTPUT);
  Serial.begin(9600);
}

void loop()
{   
  long middleDuration, middleDistance, leftDistance, rightDistance, bottomLeftDis, bottomRightDis;

  /*digitalWrite(middleTrigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(middleTrigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(middleTrigPin, LOW);    
  middleDuration = pulseIn(middleEchoPin, HIGH);
  middleDistance = middleDuration/58.2;*/

  middleDistance = digitalRead(middleIRPin);
  rightDistance  = digitalRead(rightIRPin);
  leftDistance   = digitalRead(leftIRPin);
  bottomLeftDis  = digitalRead(bottomLeftIR);
  bottomRightDis = digitalRead(bottomRightIR);
 
  /*Serial.print("Right Distance: ");
  Serial.print(rightDistance);
  Serial.print("\n");
  Serial.print("Left Distance: ");
  Serial.print(leftDistance);
  Serial.print("\n");
  Serial.print("Bottom Right Distance: ");
  Serial.print(bottomRightDis);
  Serial.print("\n");
  Serial.print("Bottom Left Distance: ");
  Serial.print(bottomLeftDis);
  Serial.print("\n");
  Serial.print("Middle Distance: ");
  Serial.print(middleDistance);
  Serial.print("\n");    
  Serial.print("------------------------------------------------------------\n\n");
  delay(1000);*/

    analogWrite(leftPulseWM,  0);  
    analogWrite(rightPulseWM, 0);
    
    digitalWrite(leftMotor1,  LOW);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, LOW);  
    digitalWrite(rightMotor2, LOW);
      
    if(middleDistance == 1)
    {
    if(rightDistance == 0 && leftDistance == 0)
    {
    analogWrite(leftPulseWM,  60);  
    analogWrite(rightPulseWM, 60);
    
    digitalWrite(leftMotor1,  HIGH);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);
    }

    if(leftDistance == 1)
    {
    analogWrite(leftPulseWM,  60);  
    analogWrite(rightPulseWM, 80);
    
    digitalWrite(leftMotor1,  HIGH);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);
    }

    if(rightDistance == 1)
    {
    analogWrite(leftPulseWM,  80);  
    analogWrite(rightPulseWM, 60);
    
    digitalWrite(leftMotor1,  HIGH);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, HIGH);  
    digitalWrite(rightMotor2, LOW);
    }
    }

    /*if(bottomRightDis == 0 && bottomLeftDis == 0 && middleDistance == 0)
    {
    analogWrite(leftPulseWM,  0);  
    analogWrite(rightPulseWM, 0); 
    
    digitalWrite(leftMotor1,  LOW);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, LOW);  
    digitalWrite(rightMotor2, LOW);

    delay(1000);

    analogWrite(leftPulseWM,  70);  
    analogWrite(rightPulseWM, 80);
    
    digitalWrite(leftMotor1,  HIGH);   
    digitalWrite(leftMotor2,  LOW); 
    digitalWrite(rightMotor1, LOW);  
    digitalWrite(rightMotor2, HIGH);

    delay(400);
    
    }*/

  delay(0);
  }
